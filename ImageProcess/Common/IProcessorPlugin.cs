﻿namespace Common
{
    public interface IProcessorPlugin
    {
        void Load(ImageProcessApp app);
        void Unload();
        void ProcessCommand(string commandName, Document doc);
        void RegisterMenu(IMenuRegistry registry);
    }
}
