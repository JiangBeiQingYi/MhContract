﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Sbk_Manage
{
    public partial class FrmHtViewer : Form
    {
        public FrmHtViewer()
        {
            InitializeComponent();
            //窗体最大化
            this.Height = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height;
            this.Width = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width;
            this.CenterToScreen();
        }
        public Image GetImage(string path)
        {
            if (!File.Exists(path))
            {
                return null;
            }
            FileStream fs = new System.IO.FileStream(path, FileMode.Open, FileAccess.Read);
            Image result = System.Drawing.Image.FromStream(fs);
            fs.Close();
            return result;
        }
        //主键
        public Int32 CurHtID = 0;
        //合同编号
        public string CurHtbh = "";
        //合同名称
        public string CurHtmc = "";
        //年份
        public string CurHtqdrq = "";
        int jpgPageindex = 0;
        //图像路径集合
        List<string> ImgLsts = new List<string>();
        private void FrmHtViewer_Load(object sender, EventArgs e)
        {
            string sqlstr = "select path,filename from multi_media_info where media_type=1 and study_no=" + CurHtID + " order by timestamp asc";
            DataTable dt = Program.SqliteDB.GetDataTable(sqlstr);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ImgLsts.Add(string.Format(@"{0}{1}\{2}", Program.APPdirPath, dt.Rows[i]["path"].ToString(), dt.Rows[i]["filename"].ToString()));
                }
            }
            btnUp.Enabled = false;
            btnDown.Enabled = false;
            jpgPageindex = 0;
            this.pictureBox1.Image = null;
            if (ImgLsts.Count > 0)
            {
                if (ImgLsts.Count == 1)
                {
                    btnDown.Enabled = false;
                    btnUp.Enabled = false;
                }
                else
                {
                    btnUp.Enabled = false;
                    btnDown.Enabled = true;
                }
                Image img = GetImage(ImgLsts[0]);
                if (img == null)
                {
                    return;
                }
                this.pictureBox1.Image = img;
                jpgPageindex = 0;
                int left = 0;
                if ((panel2.Width - img.Width) / 2 < 0)
                {
                    left = 0;
                }
                else
                {
                    left = (panel2.Width - img.Width) / 2;
                }
                this.pictureBox1.Location = new Point(left, 0);
                pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
            }

            this.panel2.AutoScroll = true;
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            this.pictureBox1.Image = null;

            if (ImgLsts.Count > 0)
            {
                jpgPageindex = jpgPageindex - 1;
                if (jpgPageindex <= 0)
                {
                    jpgPageindex = 0;
                    btnUp.Enabled = false;
                    btnDown.Enabled = true;
                }
                if (jpgPageindex == ImgLsts.Count - 1)
                {
                    btnDown.Enabled = false;
                }
                if (jpgPageindex < ImgLsts.Count - 1)
                {
                    btnDown.Enabled = true;
                }
                if (jpgPageindex > 0 && jpgPageindex < ImgLsts.Count - 1)
                {
                    btnUp.Enabled = true;
                }
                Image img = GetImage(ImgLsts[jpgPageindex]);
                if (img == null)
                {
                    return;
                }
                this.pictureBox1.Image = img;

                int left = 0;
                if ((panel2.Width - img.Width) / 2 < 0)
                {
                    left = 0;
                }
                else
                {
                    left = (panel2.Width - img.Width) / 2;
                }
                this.pictureBox1.Location = new Point(left, 0);
                pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
            }

        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            this.pictureBox1.Image = null;

            if (ImgLsts.Count > 0)
            {
                jpgPageindex = jpgPageindex + 1;
                if (jpgPageindex >= ImgLsts.Count - 1)
                {
                    jpgPageindex = ImgLsts.Count - 1;
                    btnDown.Enabled = false;
                    btnUp.Enabled = true;
                }
                if (jpgPageindex == 0)
                {
                    btnUp.Enabled = false;
                }
                if (jpgPageindex < ImgLsts.Count - 1)
                {
                    btnDown.Enabled = true;
                }
                if (jpgPageindex > 0 && jpgPageindex < ImgLsts.Count - 1)
                {
                    btnUp.Enabled = true;
                }
                Image img = GetImage(ImgLsts[jpgPageindex]);
                if (img == null)
                {
                    return;
                }
                this.pictureBox1.Image = img;

                int left = 0;
                if ((panel2.Width - img.Width) / 2 < 0)
                {
                    left = 0;
                }
                else
                {
                    left = (panel2.Width - img.Width) / 2;
                }
                this.pictureBox1.Location = new Point(left, 0);
                pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
            }
        }
    }
}
