﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;

namespace Sbk_Manage
{
    public partial class Frm_EditHt : Form
    {
        public Frm_EditHt()
        {
            InitializeComponent();
            this.KeyPreview = true;
            //修改日期格式
            Thread.CurrentThread.CurrentCulture = new CultureInfo("zh-CN");
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd HH:mm:ss";
        }
        //主键
       public Int32 CurHtID = 0;
        //合同编号
       public string CurHtbh = "";
        //合同名称
       public string CurHtmc = "";
        private void Frm_AddHt_Load(object sender, EventArgs e)
        {
            string sqlstr = "select id,htbh,xmbh,zbdlgs,htmc,ghs,hte,strftime('%Y-%m-%d',htqdrq) as htqdrq,strftime('%Y-%m-%d',htysrq) as htysrq,ghskpxx,strftime('%Y-%m-%d %H:%M:%S',htlrrq) as htlrrq,htlrr from ht_info where del_flag=0 and id=" + CurHtID;
            DataTable dt = Program.SqliteDB.GetDataTable(sqlstr);
            if (dt.Rows.Count > 0)
            {
                txtHtbh.Text = dt.Rows[0]["htbh"].ToString();
                txtHtmc.Text = dt.Rows[0]["htmc"].ToString();
                txtXmbh.Text = dt.Rows[0]["xmbh"].ToString();
                txtZbdlgs.Text = dt.Rows[0]["zbdlgs"].ToString();
                txtGhs.Text = dt.Rows[0]["ghs"].ToString();
                TxtHte.Text = dt.Rows[0]["hte"].ToString();
                txtLrr.Text = dt.Rows[0]["htlrr"].ToString();
                txtGhsYhk.Text = dt.Rows[0]["ghskpxx"].ToString();
                dtpHtQdrq.Value = Convert.ToDateTime(dt.Rows[0]["htqdrq"].ToString());
                if (dt.Rows[0]["htysrq"].ToString().Equals(""))
                {
                    chKYys.Checked = false;
                }
                else
                {
                    chKYys.Checked =true;
                    dtpHtYsrq.Value = Convert.ToDateTime(dt.Rows[0]["htysrq"].ToString());
                }
            }
            else
            {
                this.Enabled = false;
            }
            TxtHte.LostFocus += new EventHandler(TxtHte_LostFocus);
        }
        ///转半角的函数(DBC case)
        ///全角空格为12288，半角空格为32
        ///其他字符半角(33-126)与全角(65281-65374)的对应关系是：均相差65248// 
        public static string ToDBC(string input)
        {
            char[] array = input.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 12288)
                {
                    array[i] = (char)32;
                    continue;
                }
                if (array[i] > 65280 && array[i] < 65375)
                {
                    array[i] = (char)(array[i] - 65248);
                }
            }
            return new string(array);
        }
        private void TxtHte_LostFocus(object sender, EventArgs e)
        {
            if (!TxtHte.Text.Trim().Equals(""))
            {
                TxtHte.Text = ToDBC(TxtHte.Text);
                if (Microsoft.VisualBasic.Information.IsNumeric(TxtHte.Text.Trim()) == false)
                {
                    TxtHte.Text = "";
                    TxtHte.Focus();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Focus();
            if (txtHtbh.Text.Trim().Equals("") )
            {
                MessageBox.Show("合同编号不允许为空!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtHtbh.Focus();
                return;
            }
            if (txtHtmc.Text.Trim().Equals(""))
            {
                MessageBox.Show("合同名称不允许为空!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtHtmc.Focus();
                return;
            }
            string sqlstr = "";
            if (chKYys.Checked == true)
            {
                sqlstr = string.Format("update ht_info set htbh='{0}',xmbh='{1}',zbdlgs='{2}',htmc='{3}',ghs='{4}',hte={5},htqdrq='{6}',htysrq='{7}',ghskpxx='{8}',htpy='{9}' where id={10}", txtHtbh.Text.Trim().Replace("'", ""), txtXmbh.Text.Trim().Replace("'", ""), txtZbdlgs.Text.Trim().Replace("'", ""), txtHtmc.Text.Trim().Replace("'", ""), txtGhs.Text.Trim().Replace("'", ""), TxtHte.Text.Trim().Replace("'", ""), dtpHtQdrq.Value.Date.ToString("s"), dtpHtYsrq.Value.Date.ToString("s"), txtGhsYhk.Text.Trim().Replace("'", ""), PinYinConverter.GetFirst(txtHtmc.Text.Trim().Replace("'", "")), CurHtID);
            }
            else
            {
                sqlstr = string.Format("update ht_info set htbh='{0}',xmbh='{1}',zbdlgs='{2}',htmc='{3}',ghs='{4}',hte={5},htqdrq='{6}',htysrq=NULL,ghskpxx='{8}',htpy='{9}' where id={10}", txtHtbh.Text.Trim().Replace("'", ""), txtXmbh.Text.Trim().Replace("'", ""), txtZbdlgs.Text.Trim().Replace("'", ""), txtHtmc.Text.Trim().Replace("'", ""), txtGhs.Text.Trim().Replace("'", ""), TxtHte.Text.Trim().Replace("'", ""), dtpHtQdrq.Value.Date.ToString("s"), dtpHtYsrq.Value.Date.ToString("s"), txtGhsYhk.Text.Trim().Replace("'", ""), PinYinConverter.GetFirst(txtHtmc.Text.Trim().Replace("'", "")), CurHtID);
            }
            if (Program.SqliteDB.ExecuteNonQuery(sqlstr) == 1)
            {
                MessageBox.Show("合同信息更新成功!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }
        }
    }
}
